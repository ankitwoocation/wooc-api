package com.woocation.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * The Class GeoConfig.
 */
@Data
@Configuration
public class MasterDataConfig {

	/** The index name. */
	@Value("${elastic.geo.index.name}")
	private String geoCityIndexName;

	/** The geo citydoc type. */
	@Value("${elastic.geo.docType}")
	private String geoCitydocType;

	@Value("${elastic.hotel.index.name}")
	private String hotelIndexName;
	
	@Value("${elastic.hotel.docType}")
	private String hotelDocType;

}

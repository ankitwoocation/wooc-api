package com.woocation.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
public class SearchFieldConfig {

	/** The geo name id. */
	@Value("${elastic.geo.field.geoNameId}")
	private String geoNameId;

	/** The geo ascii name. */
	@Value("${elastic.geo.field.asciiname}")
	private String geoAsciiName;

	/** The geo country name. */
	@Value("${elastic.geo.field.countryname}")
	private String geoCountryName;

	/** The geo country code. */
	@Value("${elastic.geo.field.countrycode}")
	private String geoCountryCode;

	/** The geo country code. */
	@Value("${elastic.geo.field.admin1code}")
	private String geoadmin1Code;

	/** The geo country code. */
	@Value("${elastic.geo.field.state}")
	private String geoState;

	/** The geo country code. */
	@Value("${elastic.geo.field.alternativenames}")
	private String geoAlterNativeNames;

	/** The geo country code. */
	@Value("${elastic.geo.field.geoLocation}")
	private String geoLocation;

	/** The hotel location. */
	@Value("${elastic.hotel.field.geoLocation}")
	private String hotelLocation;

}

package com.woocation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.woocation.service.GeoDataUploadService;
import com.woocation.service.request.DataUploadRequest;
import com.woocation.service.response.GeoDataUploadResponse;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class GeoDataUploadController {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(GeoDataUploadController.class);

	@Autowired
	private GeoDataUploadService geoDataUploadService;

	/**
	 * Search query time.
	 *
	 * @param flightQuery
	 *            the flight query
	 * @return the response entity
	 */
	@ApiOperation(value = "Geo Data Upload", notes = "Geo Data Upload")
	@RequestMapping(value = "/uploadGeoData", method = RequestMethod.POST)
	public ResponseEntity<GeoDataUploadResponse> uploadGeoData(@RequestBody DataUploadRequest uploadRequest) {
		GeoDataUploadResponse uploadResponse = null;
		try {
			uploadResponse = geoDataUploadService.uploadGeoData(uploadRequest);
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(uploadResponse, HttpStatus.OK);
	}
}

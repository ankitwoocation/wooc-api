package com.woocation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.woocation.inference.response.InferenceResponse;
import com.woocation.service.TravelService;
import com.woocation.service.request.TravelRequest;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class TravelController {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(TravelController.class);

	@Autowired
	private TravelService travelService;

	/**
	 * Gets the travel details.
	 *
	 * @param request
	 *            the request
	 * @return the travel details
	 */
	@ApiOperation(value = "Travel Request", notes = "Travel Information on basis of TradeDetails")
	@RequestMapping(value = "/getTravelDetails", method = RequestMethod.POST)
	public ResponseEntity<?> getTravelDetails(@RequestBody TravelRequest request) {
		InferenceResponse response = null;
		try {
			response = travelService.getTravelDetails(request);
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Travel details.
	 *
	 * @param request
	 *            the request
	 * @return the response entity
	 */
	/*
	 * @ApiOperation(value = "Travel Request compacted", notes =
	 * "Travel Information on basis of TradeDetails")
	 * 
	 * @RequestMapping(value = "/travelDetails", method = RequestMethod.POST)
	 * public ResponseEntity<?> travelDetails(@RequestBody TravelRequest
	 * request) { InferenceResponse response = null; try { TravelCityDetails
	 * travelDetailsResponse = travelService.getTravelDetails(request); //
	 * response = TravelCompactedConversion.getCompactedTravelDetailsResponse(
	 * travelDetailsResponse); } catch (Exception e) {
	 * logger.error(e.getMessage()); return new ResponseEntity<>(e.getMessage(),
	 * HttpStatus.BAD_REQUEST); } return new ResponseEntity<>(response,
	 * HttpStatus.OK); }
	 */

	/**
	 * Gets the travel details get.
	 *
	 * @param frId
	 *            the fr id
	 * @param toId
	 *            the to id
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param hotelName
	 *            the hotel name
	 * @param fromLocation
	 *            the from location
	 * @param fromIP
	 *            the from IP
	 * @param hotelLocation
	 *            the hotel location
	 * @param adultCount
	 *            the adult count
	 * @param childCount
	 *            the child count
	 * @param travelPurpose
	 *            the travel purpose
	 * @param compact
	 *            the compact
	 * @return the travel details get
	 */
	@ApiOperation(value = "Travel Request", notes = "Travel Information on basis of TradeDetails")
	@RequestMapping(value = "/getTravelDetails", method = RequestMethod.GET)
	public ResponseEntity<?> getTravelDetailsGet(@RequestParam(value = "frId", required = false) Long frId,
			@RequestParam(value = "toId", required = false) Long toId, @RequestParam(value = "s_Dt") String startDate,
			@RequestParam(value = "e_Dt") String endDate,
			@RequestParam(value = "hn", required = false) String hotelName,
			@RequestParam(value = "frLoc", required = false) String fromLocation,
			@RequestParam(value = "frIP", required = false) String fromIP,
			@RequestParam(value = "hLoc", required = false) String hotelLocation,
			@RequestParam(value = "a_count", required = false) Integer adultCount,
			@RequestParam(value = "c_count", required = false) Integer childCount,
			@RequestParam(value = "c_age", required = false) Integer childAge,
			@RequestParam(value = "v_pur", required = false) String travelPurpose,
			@RequestParam(value = "relation", required = false) String relation,
			@RequestParam(value = "compact", required = false, defaultValue = "false") Boolean compact) {
		InferenceResponse response = null;
		try {
			TravelRequest request = new TravelRequest();
			request.setToGeoNameId(toId);
			request.setFromGeoNameId(frId);
			request.setStartDate(startDate);
			request.setEndDate(endDate);
			request.setHotelName(hotelName);
			request.setFromLocation(fromLocation);
			request.setFromIPAddress(fromIP);
			request.setHotelLocation(hotelLocation);
			request.setAdultCount(adultCount);
			request.setChildCount(childCount);
			request.setChildAge(childAge);
			request.setRelation(relation);
			response = travelService.getTravelDetails(request);

			// if (compact) {
			// TravelCityDetailsCompact compactResponse =
			// TravelCompactedConversion
			// .getCompactedTravelDetailsResponse(response);
			// return new ResponseEntity<>(compactResponse, HttpStatus.OK);
			// }
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}

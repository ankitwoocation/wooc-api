package com.woocation.dao;

import java.util.List;

import org.elasticsearch.action.search.SearchResponse;

import com.woocation.exception.ElasticException;
import com.woocation.model.CityEsBean;

/**
 * The Interface GeoDataSearchDao.
 */
public interface GeoDataSearchDao {

	/**
	 * Gets the city.
	 *
	 * @param geoNameId
	 *            the geo name id
	 * @return the city
	 * @throws ElasticException
	 *             the elastic exception
	 */
	public CityEsBean getCity(final Long geoNameId) throws Exception;

	/**
	 * Search city by geo point.
	 *
	 * @param geoPoint
	 *            the geo point
	 * @param distance
	 *            the distance
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	public List<CityEsBean> searchCityByGeoPoint(final String geoPoint, final String distance) throws Exception;

	/**
	 * Search City by name.
	 *
	 * @param asciiName
	 *            the ascii name
	 * @return the city es bean
	 * @throws ElasticException
	 *             the elastic exception
	 */
	public SearchResponse cityByName(final String asciiName) throws ElasticException;

	/**
	 * Search City by name.
	 *
	 * @param asciiName
	 *            the ascii name
	 * @param suggestName
	 *            the suggest name
	 * @return the city es bean
	 * @throws ElasticException
	 *             the elastic exception
	 */
	public SearchResponse citySuggest(String asciiName, String suggestName) throws ElasticException;
}

package com.woocation.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.collect.Lists;
import com.woocation.configuration.MasterDataConfig;
import com.woocation.configuration.SearchFieldConfig;
import com.woocation.dao.HotelSearchDao;
import com.woocation.elastic.bean.EsSuggest;
import com.woocation.elastic.builder.CompletionBuilder;
import com.woocation.elastic.builder.EsQuery;
import com.woocation.elastic.builder.EsQuery.EsQueryIntMode;
import com.woocation.elastic.builder.QueryBuilderHelper;
import com.woocation.elastic.builder.WooAggregationBuilderHelper;
import com.woocation.elastic.core.ElasticEntityManager;
import com.woocation.elastic.core.EntityMapper;
import com.woocation.elastic.enums.EsModeQuery;
import com.woocation.exception.ElasticException;
import com.woocation.model.BookingHotel;

@Repository
public class HotelSearchDaoImpl implements HotelSearchDao {

	/** The geo config. */
	@Autowired
	private MasterDataConfig masterConfig;

	@Autowired
	private SearchFieldConfig fieldConfig;

	/** The entity manager. */
	@Autowired
	private ElasticEntityManager entityManager;

	public HotelSearchDaoImpl() {

	}

	@Override
	public List<BookingHotel> searchHotelByGeoPoint(String hotelText, String geoPoint, String distance)
			throws Exception {

		List<BookingHotel> hotelList = new ArrayList<>();
		List<EsQuery> hotelSearchQuery = new ArrayList<>();
		EsQuery query = new EsQuery(fieldConfig.getHotelLocation(), geoPoint, distance, EsModeQuery.GEO_DISTANCE,
				EsQueryIntMode.MUST);
		hotelSearchQuery.add(query);

		if (StringUtils.isNotBlank(hotelText)) {
			query = new EsQuery("name", hotelText, EsModeQuery.MATCH, EsQueryIntMode.SHOULD);
			hotelSearchQuery.add(query);
		}

		QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(hotelSearchQuery, EsQueryIntMode.FILTER);
		SearchResponse response = entityManager.executeQuery(searchQueryBuilder, null, 0, 10,
				masterConfig.getHotelIndexName(), null, null);
		SearchHit[] dataList = response.getHits().getHits();
		for (SearchHit hit : dataList) {
			hotelList.add(EntityMapper.getInstance().getObject(hit.getSourceAsString(), BookingHotel.class));
		}
		return hotelList;
	}

	@Override
	public List<BookingHotel> hotelSuggest(String hotelText) throws Exception {
//		EsSuggest suggest = new EsSuggest("hotelSuggest", hotelText, "", null);
//		List<EsSuggest> listSuggest = Lists.newArrayList(suggest);
//		CompletionBuilder completionBuilder = WooAggregationBuilderHelper.suggest(listSuggest);
//		return entityManager.executeSuggest(masterConfig.getHotelIndexName(), completionBuilder);
		List<BookingHotel> hotelList = new ArrayList<>();
		EsQuery query = new EsQuery("hotelSuggest", hotelText , EsModeQuery.QUERY_STRING_QUERY,
				EsQueryIntMode.MUST);

		QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(Lists.newArrayList(query), EsQueryIntMode.MUST);
		SearchResponse response = entityManager.executeQuery(searchQueryBuilder, null, 0, 10,
				masterConfig.getHotelIndexName(), null, null);
		SearchHit[] dataList = response.getHits().getHits();
		for (SearchHit hit : dataList) {
			hotelList.add(EntityMapper.getInstance().getObject(hit.getSourceAsString(), BookingHotel.class));
		}
		return hotelList;
	}

}

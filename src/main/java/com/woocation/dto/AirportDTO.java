package com.woocation.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.base.Joiner;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class AirportDTO {

	/** The iata. */
	private String iata;

	/** The domestic count. */
	private Integer domesticCount;

	/** The international count. */
	private Integer internationalCount;

	@Override
	public String toString() {
		return Joiner.on("|").join(iata, domesticCount, internationalCount);
	}
}

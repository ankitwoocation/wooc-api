package com.woocation.dto;

import com.woocation.elastic.bean.EsBean;

/**
 * The Class CitySuggestBean.
 */
public class CitySuggestDTO extends EsBean {

	/** The geoname id. */
	private Object geonameId;

	/** The name. */
	private String name;
	
	/** The state. */
	private String state;
	
	/** The country code. */
	private String countryCode;

	/** The country code iso 3. */
	private String countryCodeIso3;
	
	/** The country name. */
	private String countryName;
	
	/** The full name. */
	private String fullName;
	
	/** The key. */
	private String searchMatchedKey;
	
	/** The lattitude. */
	private Object geoLocation;
	
	/** The population. */
	private Object population;
	
	/**
	 * Instantiates a new city suggest bean.
	 */
	public CitySuggestDTO(){
		
	}
	
	/**
	 * Instantiates a new city suggest bean.
	 *
	 * @param geoNameId the geo name id
	 * @param name the name
	 * @param countryCode the country code
	 */
	public CitySuggestDTO(Object geoNameId, String name, String state, String countryCode, String countryCodeIso3, String countryName) {
		this.geonameId = geoNameId;
		this.name = name;
		this.state = state;
		this.countryCode = countryCode;
		this.countryCodeIso3 = countryCodeIso3;
		this.countryName = countryName;
	}

	/**
	 * Gets the geoname id.
	 *
	 * @return the geonameId
	 */
	public Object getGeonameId() {
		return geonameId;
	}

	/**
	 * Sets the geoname id.
	 *
	 * @param geonameId the geonameId to set
	 */
	public void setGeonameId(Object geonameId) {
		this.geonameId = geonameId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the country code.
	 *
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the country code iso 3.
	 *
	 * @return the countryCodeIso3
	 */
	public String getCountryCodeIso3() {
		return countryCodeIso3;
	}

	/**
	 * Sets the country code iso 3.
	 *
	 * @param countryCodeIso3 the countryCodeIso3 to set
	 */
	public void setCountryCodeIso3(String countryCodeIso3) {
		this.countryCodeIso3 = countryCodeIso3;
	}

	/**
	 * Gets the country name.
	 *
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * Sets the country name.
	 *
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the searchMatchedKey
	 */
	public String getSearchMatchedKey() {
		return searchMatchedKey;
	}

	/**
	 * @param searchMatchedKey the searchMatchedKey to set
	 */
	public void setSearchMatchedKey(String searchMatchedKey) {
		this.searchMatchedKey = searchMatchedKey;
	}

	/**
	 * @return the population
	 */
	public Object getPopulation() {
		return population;
	}

	/**
	 * @param population the population to set
	 */
	public void setPopulation(Object population) {
		this.population = population;
	}

	/**
	 * @return the geoLocation
	 */
	public Object getGeoLocation() {
		return geoLocation;
	}

	/**
	 * @param geoLocation the geoLocation to set
	 */
	public void setGeoLocation(Object geoLocation) {
		this.geoLocation = geoLocation;
	}
}

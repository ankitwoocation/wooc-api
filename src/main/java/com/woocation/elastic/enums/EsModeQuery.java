package com.woocation.elastic.enums;

/**
 * The Enum EsModeQuery.
 * 
 * @author ankit.gupta4
 */
public enum EsModeQuery {

	/** The match. */
	MATCH,
	
	/** The range. */
	RANGE,
	
	GEO_DISTANCE,
	
	/** The term. */
	TERM,
	
	/** The query string query. */
	QUERY_STRING_QUERY,
	
	/** The function score. */
	FUNCTION_SCORE;
}
package com.woocation.elastic.enums;

public enum FieldType {

	ALPHA,
	
	NUMERIC,
	
	LAT,
	
	LAN,
	
	DATE;

}

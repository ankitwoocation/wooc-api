package com.woocation.inference;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * The Enum TravelType.
 */
public enum TravelType {

	/** The one night. */
	ONE_NIGHT("one_night"),

	/** The short stay. */
	SHORT_STAY("short_stay"),

	/** The medium stay. */
	MEDIUM_STAY("medium_stay"),

	/** The long stay. */
	LONG_STAY("long_stay"),

	/** The weekend stay. */
	WEEKEND_STAY("weekend"),

	/** The holiday stay. */
	HOLIDAY_STAY("holiday");

	private List<String> travelTags = null;

	/**
	 * Instantiates a new traveller type.
	 *
	 * @param tags
	 *            the tags
	 */
	private TravelType(String... tags) {
		travelTags = Lists.newArrayList(tags);
	}

	/**
	 * @return the travelTags
	 */
	public List<String> getTravelTags() {
		return travelTags;
	}
}

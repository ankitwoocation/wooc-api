package com.woocation.inference;

import java.util.List;

import com.google.common.collect.Lists;


/**
 * The Enum TravellerType.
 */
public enum TravellerType {

	/** The solo. */
	SOLO("solo"),

	/** The couple. */
	COUPLE("couple"),

	/** The group. */
	GROUP("group", "friends"),

	/** The infant. */
	INFANT("infant"),

	/** The family. */
	FAMILY("family"),

	/** The children. */
	CHILDREN("kids");

	private List<String> travellerTags = null;

	/**
	 * Instantiates a new traveller type.
	 *
	 * @param tags
	 *            the tags
	 */
	private TravellerType(String... tags) {
		travellerTags = Lists.newArrayList(tags);
	}

	/**
	 * @return the travellerTags
	 */
	public List<String> getTravellerTags() {
		return travellerTags;
	}
	
}

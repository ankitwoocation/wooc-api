package com.woocation.inference;

/**
 * The Enum WeatherConcern.
 */
public enum WeatherConcern {

	/** The low. */
	LOW,
	
	/** The medium. */
	MEDIUM,
	
	/** The high. */
	HIGH,
	
	/** The major. */
	VERY_HIGH
	
}

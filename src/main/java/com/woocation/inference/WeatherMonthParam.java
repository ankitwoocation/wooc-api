package com.woocation.inference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Instantiates a new weather month param.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherMonthParam {

	/** The min. */
	private Integer min;
	
	/** The max. */
	private Integer max;
	
	/** The snow. */
	private Boolean snow;
	
	/** The rain. */
	private Boolean rain;
	
	/** The month. */
	private Integer month;

}

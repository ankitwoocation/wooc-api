package com.woocation.inference.extracter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;
import com.woocation.inference.Concern;
import com.woocation.inference.TravelType;
import com.woocation.inference.TravellerType;
import com.woocation.inference.WeatherCityParam;
import com.woocation.inference.WeatherConcern;
import com.woocation.inference.response.InferenceParam;
import com.woocation.inference.response.TravelCityDetails;
import com.woocation.inference.response.TravelCityParam;
import com.woocation.model.CityEsBean;
import com.woocation.model.WeatherTypeParam;
import com.woocation.service.request.TravelRequest;
import com.woocation.util.DistanceCalculator;

/**
 * The Class InferenceExtracter.
 */
@Component
public class InferenceExtracter {

	/**
	 * Extract inference details.
	 *
	 * @param travelRequest
	 *            the travel request
	 * @param cityDetails
	 *            the city details
	 * @param fromCity
	 *            the from city
	 * @param toCity
	 *            the to city
	 * @return the inference param
	 */
	public InferenceParam extractInferenceDetails(TravelRequest travelRequest, TravelCityParam travelCityParam,
			CityEsBean fromCity, CityEsBean toCity) {
		InferenceParam inferParam = new InferenceParam();
		setTravelType(travelRequest, inferParam);
		setTravellerType(travelRequest, inferParam);
		setWeatherAttributes(travelCityParam, inferParam);
		setDistance(fromCity, toCity, inferParam);
		setMountain(travelCityParam, inferParam);
		setConcerns(travelCityParam, inferParam, toCity);
		inferParam.setDuration(travelRequest.getDuration());
		inferParam.setFromCityLongWeekend(travelCityParam.getFromCityDetails().getCityLongWeekend());
		inferParam.setToCityLongWeekend(travelCityParam.getToCityDetails().getCityLongWeekend());
		setWeatherConcerns(travelCityParam, inferParam);
		taxiAvailable(travelCityParam, inferParam);
		setNetworkAvailable(travelCityParam, inferParam);
		setThreat(travelCityParam, inferParam);
		setSubway(travelCityParam, inferParam);
		setCuisines(travelCityParam, inferParam);
		return inferParam;
	}

	private void setCuisines(TravelCityParam cityParams, InferenceParam inferParam) {
		List<String> fromCuisine = cityParams.getFromCityDetails().getCuisines();
		List<String> toCuisine = cityParams.getToCityDetails().getCuisines();

		List<String> finalList = new ArrayList<>();
		fromCuisine.stream().forEach(item -> {
			if (toCuisine.contains(item)) {
				finalList.add(item);
			}
		});
		if (finalList.size() > 5) {
			inferParam.setCuisines(finalList.subList(0, 5));
		} else {
			inferParam.setCuisines(finalList);
		}
	}

	/**
	 * Sets the subway.
	 *
	 * @param cityParams
	 *            the city params
	 * @param inferParam
	 *            the infer param
	 */
	private void setSubway(TravelCityParam cityParams, InferenceParam inferParam) {
		if (cityParams.getToCityDetails().getHasSubway() != null) {
			inferParam.setSubway(false);
		} else {
			inferParam.setSubway(cityParams.getToCityDetails().getHasSubway());
		}
	}

	/**
	 * Sets the threat.
	 *
	 * @param cityParams
	 *            the city params
	 * @param response
	 *            the response
	 */
	private void setThreat(TravelCityParam cityParams, InferenceParam response) {
		Double from = cityParams.getFromCityDetails().getCityThreat();
		Double to = cityParams.getToCityDetails().getCityThreat();
		// Less - High

		if (from != null && to != null) {
			Double difference = from - to;
			if (difference > 0) {
				if (difference < 20) {
					response.setThreatConcern(Concern.LOW.name());
				}

				if (difference < 30) {
					response.setThreatConcern(Concern.MEDIUM.name());
				}

				if (difference < 40) {
					response.setThreatConcern(Concern.HIGH.name());
				}

				if (difference < 50) {
					response.setThreatConcern(Concern.CRITICAL.name());
				}
			} else {
				response.setThreatConcern(Concern.NO_CONERN.name());
			}
		} else {
			response.setThreatConcern(Concern.NO_CONERN.name());
		}
	}

	/**
	 * Sets the network available.
	 *
	 * @param cityParams
	 *            the city params
	 * @param response
	 *            the response
	 */
	private void setNetworkAvailable(TravelCityParam cityParams, InferenceParam response) {
		TravelCityDetails fromCityDetails = cityParams.getFromCityDetails();
		TravelCityDetails toCityDetails = cityParams.getToCityDetails();

		if (CollectionUtils.isNotEmpty(toCityDetails.getNetworkOperator())) {
			if (CollectionUtils.isNotEmpty(fromCityDetails.getNetworkOperator())) {
				List<String> common = new ArrayList<>(toCityDetails.getNetworkOperator());
				common.retainAll(fromCityDetails.getNetworkOperator());
				if (CollectionUtils.isNotEmpty(common)) {
					response.setNetworkAvailable(Joiner.on(",").join(common));
				} else {
					response.setNetworkAvailable(Joiner.on(",").join(toCityDetails.getNetworkOperator()));
				}
			} else {
				response.setNetworkAvailable(Joiner.on(",").join(toCityDetails.getNetworkOperator()));
			}
		}
	}

	/**
	 * Taxi available.
	 *
	 * @param cityDetails
	 *            the city details
	 * @param response
	 *            the response
	 */
	private void taxiAvailable(TravelCityParam cityParams, InferenceParam response) {
		TravelCityDetails fromCityDetails = cityParams.getFromCityDetails();
		TravelCityDetails toCityDetails = cityParams.getToCityDetails();

		if (CollectionUtils.isNotEmpty(toCityDetails.getCabs())) {
			if (CollectionUtils.isNotEmpty(fromCityDetails.getCabs())) {
				List<String> common = new ArrayList<>(toCityDetails.getCabs());
				common.retainAll(fromCityDetails.getCabs());
				if (CollectionUtils.isNotEmpty(common)) {
					response.setTaxiAvailable(Joiner.on(",").join(common));
				} else {
					response.setTaxiAvailable(Joiner.on(",").join(toCityDetails.getCabs()));
				}
			} else {
				response.setTaxiAvailable(Joiner.on(",").join(toCityDetails.getCabs()));
			}
		}
	}

	/**
	 * Sets the concerns.
	 *
	 * @param cityDetails
	 *            the city details
	 * @param response
	 *            the response
	 */
	private void setConcerns(TravelCityParam cityParams, InferenceParam response, CityEsBean toCity) {
		boolean sameCountry = cityParams.getFromCityDetails().getCountry()
				.equalsIgnoreCase(cityParams.getToCityDetails().getCountry());
		boolean sameState = false;

		if (sameCountry) {
			sameState = cityParams.getFromCityDetails().getState()
					.equalsIgnoreCase(cityParams.getToCityDetails().getState());
		} else {
			response.setInternational(true);
		}

		if (cityParams.getToCityDetails().getElevation() != null) {
			if (Double.valueOf(cityParams.getToCityDetails().getElevation()) > 2000) {
				response.setElevationConcern(true);
			}
		}

		boolean smallCity = checkSmallCity(toCity);
		if (sameState) {
			response.setFoodConcern(Concern.NO_CONERN.name());
			response.setConnectivityConcern(Concern.LOW.name());
		}

		if (sameCountry && !sameState) {
			response.setFoodConcern(Concern.LOW.name());
			response.setConnectivityConcern(Concern.LOW.name());
		}

		if (!sameCountry) {
			response.setFoodConcern(Concern.MEDIUM.name());
			response.setConnectivityConcern(Concern.HIGH.name());
		}

		if (!sameCountry && smallCity) {
			response.setFoodConcern(Concern.HIGH.name());
			response.setConnectivityConcern(Concern.CRITICAL.name());
		}

		if (cityParams.getToCityDetails().getNetwork() != null
				&& cityParams.getToCityDetails().getNetwork().getCoverage() < 70) {
			response.setConnectivityConcern(Concern.CRITICAL.name());
		}
	}

	/**
	 * Check small city.
	 *
	 * @param cityDetails
	 *            the city details
	 * @param toCity
	 *            the to city
	 * @return true, if successful
	 */
	private boolean checkSmallCity(CityEsBean toCity) {
		return toCity.getPopulation().intValue() < 25000;
	}

	/**
	 * Sets the traveller type.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	private void setTravellerType(TravelRequest request, InferenceParam response) {
		Integer childCount = request.getChildCount();
		Integer adultCount = request.getAdultCount();

		Boolean couple = StringUtils.isNotEmpty(request.getRelation()) && request.getRelation().equalsIgnoreCase("C")
				? true : false;

		TravellerType type = null;

		if (childCount == null) {
			childCount = 0;
		}

		if (adultCount != null) {
			if (adultCount == 1 && childCount == 0) {
				type = TravellerType.SOLO;
			}

			if (couple && adultCount == 2 && childCount == 0) {
				type = TravellerType.COUPLE;
			}

			if (!couple && adultCount == 2 && childCount == 0) {
				type = TravellerType.COUPLE;
			}

			if (adultCount > 2 && childCount == 0) {
				type = TravellerType.GROUP;
			}

			if (adultCount >= 1 && childCount > 0) {
				type = TravellerType.FAMILY;
			}
		}
		response.setTravellerType(type);
	}

	/**
	 * Calculate traveller type.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 */
	private void setTravelType(TravelRequest request, InferenceParam response) {
		Integer duration = request.getDuration();
		TravelType type = null;

		if (duration == 1) {
			type = TravelType.ONE_NIGHT;
		}

		if (duration > 2 && duration < 7) {
			type = TravelType.SHORT_STAY;
		}

		if (duration > 4 && duration < 15) {
			type = TravelType.MEDIUM_STAY;
		}

		if (duration > 15) {
			type = TravelType.LONG_STAY;
		}

		response.setTravelType(type);
	}

	/**
	 * Sets the mountain.
	 *
	 * @param cityDetails
	 *            the new mountain
	 * @param response
	 *            the response
	 */
	private void setMountain(TravelCityParam travelCityParam, InferenceParam response) {
		if (!travelCityParam.getFromCityDetails().getHasMountain()
				&& travelCityParam.getToCityDetails().getHasMountain()) {
			response.setGoingToMountain(true);
		} else {
			response.setGoingToMountain(false);
		}
	}

	/**
	 * Sets the weather attributes.
	 *
	 * @param cityParams
	 *            the city params
	 * @param response
	 *            the response
	 */
	private void setWeatherAttributes(TravelCityParam cityParams, InferenceParam response) {
		TravelCityDetails fromCityDetails = cityParams.getFromCityDetails();
		TravelCityDetails toCityDetails = cityParams.getToCityDetails();

		Map<String, WeatherTypeParam> fromWeather = fromCityDetails.getWeather();
		Map<String, WeatherTypeParam> toWeather = toCityDetails.getWeather();
		WeatherTypeParam snowDays = toWeather.get("Snow days");
		if (snowDays.getValue() != null && (double) snowDays.getValue() > 0) {
			response.setSnowPossible(true);
		} else {
			response.setSnowPossible(false);
		}

		WeatherTypeParam fromHotDays = fromWeather.get("Mean daily maximum");
		WeatherTypeParam toHotDays = toWeather.get("Mean daily maximum");
		
		Integer toCurrentTemp = (int) toHotDays.getValue();

		int tempDifference = (int) fromHotDays.getValue() - (int) toHotDays.getValue();
		if (tempDifference > 10 && toCurrentTemp < 15) {
			response.setGoingToHot(false);
			response.setGoingToCold(true);
		}

		if (tempDifference < -10 && toCurrentTemp > 35) {
			response.setGoingToHot(true);
			response.setGoingToCold(false);
		}
	}

	/**
	 * Sets the weather concerns.
	 *
	 * @param cityParams
	 *            the city params
	 * @param response
	 *            the response
	 */
	private void setWeatherConcerns(TravelCityParam cityParams, InferenceParam response) {
		TravelCityDetails fromCityDetails = cityParams.getFromCityDetails();
		TravelCityDetails toCityDetails = cityParams.getToCityDetails();

		WeatherCityParam fromWeather = fromCityDetails.getWeatherCityParam();
		WeatherCityParam toWeather = toCityDetails.getWeatherCityParam();

		if (toWeather.getWt_12_max() - fromWeather.getWt_12_max() > 15) {
			response.setHotConcern(WeatherConcern.VERY_HIGH.name());
		} else {
			int difference = toWeather.getWt_past_1_max() - fromWeather.getWt_past_1_max();

			if (difference >= 20) {
				response.setHotConcern(WeatherConcern.HIGH.name());
			}

			if (difference > 10 && difference < 20) {
				response.setHotConcern(WeatherConcern.MEDIUM.name());
			}
		}

		int yearDifference = 0;
		if (toWeather.getWt_12_min() < 0) {
			yearDifference = toWeather.getWt_12_min() + fromWeather.getWt_12_min();
		} else {
			yearDifference = toWeather.getWt_12_min() - fromWeather.getWt_12_min();
		}
		if (yearDifference < -15) {
			response.setColdConcern(WeatherConcern.VERY_HIGH.name());
		} else {
			int difference = toWeather.getWt_past_1_min() - fromWeather.getWt_past_1_min();

			if (difference > 15) {
				response.setColdConcern(WeatherConcern.HIGH.name());
			}

			if (difference > 10 && difference < 20) {
				response.setColdConcern(WeatherConcern.MEDIUM.name());
			}
		}

		if (toCityDetails.getWeatherCityParam().getWt_current_rain() != null
				&& toCityDetails.getWeatherCityParam().getWt_current_rain()) {
			response.setRainPossible(true);
		}

	}

	/**
	 * Sets the distance.
	 *
	 * @param fromCity
	 *            the from city
	 * @param toCity
	 *            the to city
	 * @param response
	 *            the response
	 */
	private void setDistance(CityEsBean fromCity, CityEsBean toCity, InferenceParam response) {
		response.setDistance(
				DistanceCalculator.distance(fromCity.getGeoLocation(), toCity.getGeoLocation(), "K") + " Kilometers");
	}

}

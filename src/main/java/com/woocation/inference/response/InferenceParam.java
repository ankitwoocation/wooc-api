/**
 * 
 */
package com.woocation.inference.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.woocation.inference.TravelType;
import com.woocation.inference.TravellerType;

import lombok.Data;
import lombok.NoArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new inference param.
 */

/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Data

/**
 * Instantiates a new inference param.
 */
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class InferenceParam {

	/** The traveller type. */
	private TravellerType travellerType;
	
	/** The travel type. */
	private TravelType travelType;
	
	/** The going to mountain. */
	private Boolean goingToMountain;
		
	/** The snow possible. */
	private Boolean snowPossible;
	
	/** The rain possible. */
	private Boolean rainPossible;
	
	/** The duration. */
	private Integer duration;

	/** The distance. */
	private String distance;

	/** The going to cold. */
	private Boolean goingToCold;

	/** The going to hot. */
	private Boolean goingToHot;
	
	/** The from city long weekend. */
	private Boolean fromCityLongWeekend;

	/** The is long weekend. */
	private Boolean toCityLongWeekend;
	
	/** The elevation concern. */
	private boolean elevationConcern;
	
	/** The food concern. */
	private String foodConcern;
	
	/** The connectivity concern. */
	private String connectivityConcern;
	
	/** The hot concern. */
	private String hotConcern;
	
	/** The cold concern. */
	private String coldConcern;
	
	/** The taxi available. */
	private String taxiAvailable; 
	
	/** The threat concern. */
	private String threatConcern;
	
	/** The network available. */
	private String networkAvailable; 
		
	/** The subway. */
	private boolean subway;
	
	/** The inference tags. */
	private List<String> inferenceTags;
	
	/** The cuisines. */
	private List<String> cuisines; 
	
	/** The international. */
	private boolean international;
	
}

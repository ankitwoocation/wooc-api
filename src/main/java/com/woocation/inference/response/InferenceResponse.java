package com.woocation.inference.response;

import java.util.List;

import com.woocation.model.AmenityDTO;
import com.woocation.model.BookingHotel;

import lombok.Data;

/**
 * The Class InferenceResponse.
 *
 * @author ankit.gupta
 */

@Data
public class InferenceResponse {

	/** The travel details response. */
	private TravelCityParam travelDetailsResponse;
	
	/** The hotel. */
	private BookingHotel hotel;
	
	/** The inference param. */
	private InferenceParam inferenceParam;
	
	/** The hotel amenity. */
	private List<AmenityDTO> hotelAmenity;

	/** The local places. */
	private Object localPlaces;
}

package com.woocation.inference.response;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.woocation.dto.AirportDTO;
import com.woocation.inference.WeatherCityParam;
import com.woocation.model.CommonDTO;
import com.woocation.model.ModeRange;
import com.woocation.model.NetworkDetail;
import com.woocation.model.WeatherTypeParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new travel city details.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TravelCityDetails extends CommonDTO {

	/** The from geo id. */
	private Long geoId;

	private String cityName;

	/** The from state. */
	private String state;

	/** The from country. */
	private String country;

	/** The from weather. */
	@JsonIgnore
	private Map<String, WeatherTypeParam> weather;

	/** The nearest big city. */
	private List<String> nearestBigCity;

	/** The from airport. */
	private List<AirportDTO> airport;

	/** The is european. */
	private Boolean european;

	/** The to city currency. */
	private String currencyCode;

	/** The is beach. */
	private Boolean hasBeach;

	/** The from mountain. */
	private Boolean hasMountain;

	/** The from city long weekend. */
	private Boolean cityLongWeekend;

	/** The from days stats. */
	private Map<String, Boolean> daysStats;

	/** The from population density. */
	private Double populationDensity;

	/** The from language. */
	private String language;

	/** The from vegetation. */
	private ModeRange vegetation;

	/** The from network. */
	private NetworkDetail network;

	/** The elevation. */
	private String elevation;

	/** The from wh city param. */
	private WeatherCityParam weatherCityParam;

	/** The from cab. */
	private List<String> cabs;

	/** The from subway. */
	private Boolean hasSubway;

	/** The from city threat. */
	private Double cityThreat;

	/** The from network operator. */
	private List<String> networkOperator;
	
	/** The cuisines. */
	private List<String> cuisines;
}

package com.woocation.inference.response;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.woocation.model.WeatherTypeParam;

import lombok.Data;

/**
 * The Class TravelDetailsResponse.
 */
@Data
public class TravelCityDetailsCompact {

	/** The from city. */
	private String fc;

	/** The from state. */
	private String tc;

	/** The from weather. */
	@JsonIgnore
	private Map<String, WeatherTypeParam> fromWeather;

	/** The to weather. */
	@JsonIgnore
	private Map<String, WeatherTypeParam> toWeather;

	/** The nearest big city. */
	private String nbc;

	/** The nearest airport. */
	private List<String> airports;

	/** The from european. */
	private Boolean f_eu;
	
	/** The t eu. */
	private Boolean t_eu;

	/** The from cur. */
	private String from_cur;
	
	/** The to city currency. */
	private String to_cur;

	/** The is beach. */
	private Boolean f_beach;
	
	/** The t beach. */
	private Boolean t_beach;
	
	/** The f mountain. */
	private Boolean f_mountain;
	
	/** The t mountain. */
	private Boolean t_mountain;

	/** The from days stats. */
	private String frStats;

	/** The to days stats. */
	private String toStats;

	/** The hotel. */
	private String hotel;
}

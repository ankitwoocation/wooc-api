package com.woocation.inference.response;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Instantiates a new travel city param.
 */
@Data
@NoArgsConstructor
public class TravelCityParam {

	/** The from city details. */
	private TravelCityDetails fromCityDetails;
	
	/** The to city details. */
	private TravelCityDetails toCityDetails;

}

package com.woocation.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The Class AmenityDTO.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class AmenityDTO extends CommonDTO {

	/** The amenity id. */
	private String amenityId;
	
	/** The amenity label. */
	private String amenityLabel;

	/** The amenity card info. */
	private String amenityAlternateLabel;

	/** The category. */
	private String category;

	/** The properties. */
	private List<String> properties;
}

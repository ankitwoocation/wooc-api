package com.woocation.model;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.woocation.elastic.bean.EsBean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The Class CityEsBean.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CityEsBean extends EsBean {

	/** The geoname id. */
	private Long geonameId;

	/** The name. */
	private String name;

	/** The ascii name. */
	private String asciiName;

	/** The ascii suggest. */
	private List<CitySuggestBean> asciiSuggest;

	/** The alternate names suggest. */
	private List<CitySuggestBean> alternateNamesSuggest;

	/** The alternate names. */
	private String alternateNames;

	/** The lattitude. */
	private Location geoLocation;

	/** The feature class. */
	private String featureClass;

	/** The feature code. */
	private String featureCode;

	/** The country code. */
	private String countryCode;

	/** The cc 2. */
	private String cc2;

	/** The admin 1 code. */
	private String admin1Code;

	/** The admin 2 code. */
	private String admin2Code;

	/** The admin 3 code. */
	private String admin3Code;

	/** The admin code. */
	private String admin4Code;

	/** The population. */
	private BigInteger population;

	/** The elevation. */
	private Integer elevation;

	/** The dem. */
	private Integer dem;

	/** The time zone. */
	private String timeZone;

	/** The time zone utc. */
	private String timeZoneUtc;

	/** The modification date. */
	private String modificationDate;

	/** The state. */
	private String state;

	/** The country name. */
	private String countryName;

	/** The country code iso 3. */
	private String countryCodeIso3;

	/** The currency name. */
	private String currencyName;

	/** The currency code. */
	private String currencyCode;

	/** The european. */
	private boolean european;

	/** The schengen. */
	private boolean schengen;

	/** The subway. */
	private Subway subway;

	/** The elevation ref. */
	private Elevation elevationRef;

	/** The network. */
	private Network network;

	/** The population ref. */
	private Population populationRef;

	/** The languages ref. */
	private Languages languagesRef;

	/** The uv details. */
	private UVBean uvDetails;

	/** The vegetation. */
	private Vegetation vegetation;

	/** The weather. */
	private Weather weather;

	/** The airports. */
	private List<Airport> airports;

	/** The holidays. */
	private List<Holiday> holidays;

	/** The is mountain. */
	private boolean isMountain;

	/** The is river. */
	private boolean isRiver;

	/** The is beach. */
	private boolean isBeach;

	/** The working days. */
	private String workingDays;

	/** The safety reference. */
	private SafetyReference safetyReference;
	
	/** The cab details. */
	private List<String> cabDetails;
	
	/** The threat ratio. */
	private Double threatRatio;
	
	/** The network operator. */
	private List<String> networkOperator;
	
	/** The google subway. */
	private Boolean googleSubway;
	
	/** The cuisines. */
	private List<List<String>> cuisines;

	/**
	 * Sets the suggest.
	 */
	public void setSuggest() {
		List<CitySuggestBean> suggestList = new ArrayList<>();
		List<CitySuggestBean> alterNamesList = new ArrayList<>();
		if (this.population.signum() > 0) {
			suggestList.add(new CitySuggestBean(this.getAsciiName(), this.population));
			suggestList.add(new CitySuggestBean(this.getAsciiName() + " " + this.getCountryCode(), this.population));
			suggestList.add(new CitySuggestBean(this.getAsciiName() + " " + this.getCountryName(), this.population));

			if (this.getAlternateNames().length() > 0) {
				String[] alternateCityArray = this.getAlternateNames().split(",");
				for (String alterCity : alternateCityArray) {
					if (alterCity.trim().length() > 0) {
						alterNamesList.add(new CitySuggestBean(StringUtils.capitalize(alterCity), this.population));
					}
				}
			}
		}
		this.setAsciiSuggest(suggestList);
		this.setAlternateNamesSuggest(alterNamesList);
	}
}

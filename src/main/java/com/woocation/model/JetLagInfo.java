/**
 * 
 */
package com.woocation.model;

/**
 * The Class JetLagInfo.
 *
 * @author ankit
 */
public class JetLagInfo {
	
	/** The to time zone. */
	private String toTimeZone;
	
	/** The from time zone. */
	private String fromTimeZone;
	
	/** The time zone difference. */
	private float timeZoneDifference;
	
	/** The direction. */
	private String direction;
	
	/** The recovery time. */
	private float recoveryTime;
	
	/**
	 * Instantiates a new jet lag info.
	 */
	public JetLagInfo() {
	}

	/**
	 * Gets the to time zone.
	 *
	 * @return the toTimeZone
	 */
	public String getToTimeZone() {
		return toTimeZone;
	}

	/**
	 * Sets the to time zone.
	 *
	 * @param toTimeZone the toTimeZone to set
	 */
	public void setToTimeZone(String toTimeZone) {
		this.toTimeZone = toTimeZone;
	}

	/**
	 * Gets the from time zone.
	 *
	 * @return the fromTimeZone
	 */
	public String getFromTimeZone() {
		return fromTimeZone;
	}

	/**
	 * Sets the from time zone.
	 *
	 * @param fromTimeZone the fromTimeZone to set
	 */
	public void setFromTimeZone(String fromTimeZone) {
		this.fromTimeZone = fromTimeZone;
	}

	/**
	 * Gets the time zone difference.
	 *
	 * @return the timeZoneDifference
	 */
	public float getTimeZoneDifference() {
		return timeZoneDifference;
	}

	/**
	 * Gets the recovery time.
	 *
	 * @return the recoveryTime
	 */
	public float getRecoveryTime() {
		return recoveryTime;
	}

	/**
	 * Sets the recovery time.
	 *
	 * @param recoveryTime the recoveryTime to set
	 */
	public void setRecoveryTime(float recoveryTime) {
		this.recoveryTime = recoveryTime;
	}

	/**
	 * Sets the time zone difference.
	 *
	 * @param timeZoneDifference the timeZoneDifference to set
	 */
	public void setTimeZoneDifference(float timeZoneDifference) {
		this.timeZoneDifference = timeZoneDifference;
	}

	/**
	 * Gets the direction.
	 *
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}

	/**
	 * Sets the direction.
	 *
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}
}

package com.woocation.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * Instantiates a new language details.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LanguageDetails {

	/** The language. */
	private String language;

	/** The language code. */
	private String languageCode;

	/** The ratio. */
	private Float ratio;

}

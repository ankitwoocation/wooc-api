package com.woocation.model;

import com.google.common.base.Joiner;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class Location.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public final class Location {

	/** The lat. */
	private Double lat;
    
    /** The lon. */
    private Double lon;
    
    @Override
	public String toString() {
		return Joiner.on(",").join(lat, lon);
	}
}

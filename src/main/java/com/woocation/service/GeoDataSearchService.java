package com.woocation.service;

import java.util.List;

import com.woocation.dto.CityDTO;
import com.woocation.dto.CityEsDTO;
import com.woocation.dto.CitySuggestDTO;
import com.woocation.dto.GeoDTO;
import com.woocation.model.JetLagInfo;
import com.woocation.service.response.GeoIPLocationResponse;
import com.woocation.util.PropertyEnum;

/**
 * The Interface GeoDataSearchService.
 */
public interface GeoDataSearchService {

	/**
	 * Search C ity.
	 *
	 * @param geoNameId
	 *            the geo name id
	 * @return the city es bean
	 */
	public CityDTO searchCity(Long geoNameId);

	/**
	 * Gets the city.
	 *
	 * @param geoNameId
	 *            the geo name id
	 * @param field
	 *            the field
	 * @return the city
	 */
	public CityEsDTO getCity(Long geoNameId, PropertyEnum field);

	/**
	 * Search City by name.
	 *
	 * @param asciiName
	 *            the ascii name
	 * @return the city es bean
	 */
	public CityDTO searchCityByName(String asciiName);

	/**
	 * Search city by country.
	 *
	 * @param asciiName
	 *            the ascii name
	 * @param country
	 *            the country
	 * @param countryCode
	 *            the country code
	 * @return the city es bean
	 */
	public List<CityDTO> searchCityByCountry(String asciiName, String country, String countryCode);

	/**
	 * Search city by geo point.
	 *
	 * @param geoPoint
	 *            the geo point
	 * @return the list
	 */
	public List<CityDTO> searchCityByGeoPoint(final String geoPoint, final String distance);

	/**
	 * Search city by IP.
	 *
	 * @param ipAddress
	 *            the ip address
	 * @return the list
	 */
	public GeoIPLocationResponse searchCityByIP(String ipAddress);

	/**
	 * Gets the jet lag.
	 *
	 * @param toGeoId
	 *            the to geo id
	 * @param fromGeoId
	 *            the from geo id
	 * @return the jet lag
	 */
	public JetLagInfo getJetLag(Long toGeoId, Long fromGeoId);

	/**
	 * City suggest.
	 *
	 * @param asciiName
	 *            the ascii name
	 * @return the city es bean
	 */
	public List<CitySuggestDTO> citySuggest(final String asciiName);

	/**
	 * Geo list.
	 *
	 * @param countryName
	 *            the country name
	 * @return the list
	 */
	public List<GeoDTO> geoList(final String countryName);

}

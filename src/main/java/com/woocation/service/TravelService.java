package com.woocation.service;

import com.woocation.inference.response.InferenceResponse;
import com.woocation.inference.response.TravelCityDetails;
import com.woocation.service.request.TravelRequest;

/**
 * The Interface TravelService.
 */
public interface TravelService {

	/**
	 * Gets the travel details.
	 *
	 * @param travelRequest
	 *            the travel request
	 * @return the travel details
	 * @throws Exception
	 *             the exception
	 */
	public InferenceResponse getTravelDetails(TravelRequest travelRequest) throws Exception;
}

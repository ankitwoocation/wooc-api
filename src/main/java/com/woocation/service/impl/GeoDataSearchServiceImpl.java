package com.woocation.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion.Entry.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.woocation.configuration.GeoSearchConfiguration;
import com.woocation.configuration.MasterDataConfig;
import com.woocation.configuration.SearchFieldConfig;
import com.woocation.dao.GeoDataSearchDao;
import com.woocation.dto.CityDTO;
import com.woocation.dto.CityEsDTO;
import com.woocation.dto.CitySuggestDTO;
import com.woocation.dto.GeoDTO;
import com.woocation.elastic.builder.EsQuery;
import com.woocation.elastic.builder.EsQuery.EsQueryIntMode;
import com.woocation.elastic.builder.QueryBuilderHelper;
import com.woocation.elastic.core.ElasticEntityManager;
import com.woocation.elastic.core.EntityMapper;
import com.woocation.elastic.enums.EsModeQuery;
import com.woocation.model.CityEsBean;
import com.woocation.model.JetLagInfo;
import com.woocation.service.GeoDataSearchService;
import com.woocation.service.response.GeoIPLocationResponse;
import com.woocation.util.EntityToDTO;
import com.woocation.util.GeoIPExtractor;
import com.woocation.util.PropertyEnum;
import com.woocation.util.WooApiUtils;

/**
 * The Class GeoDataSearchServiceImpl.
 */
@Service
public class GeoDataSearchServiceImpl implements GeoDataSearchService {

	/** The geo config. */
	@Autowired
	private MasterDataConfig masterConfig;

	@Autowired
	private SearchFieldConfig fieldConfig;

	@Autowired
	private GeoSearchConfiguration geoConfig;

	@Autowired
	private GeoDataSearchDao geoDataSearchDao;
	
	@Autowired
	private GeoIPExtractor geoIPExtracter;

	/** The entity manager. */
	@Autowired
	private ElasticEntityManager entityManager;

	public CityDTO searchCity(Long geoNameId) {
		CityDTO cityDto = null;
		try {
			CityEsBean cityBean = geoDataSearchDao.getCity(geoNameId);
			if (cityBean != null) {
				cityDto = EntityToDTO.getInstance().getCityBean(cityBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cityDto;
	}

	public CityEsDTO getCity(Long geoNameId, PropertyEnum field) {
		CityEsDTO cityEsDto = null;
		try {
			CityEsBean cityBean = geoDataSearchDao.getCity(geoNameId);
			if (cityBean != null) {
				cityEsDto = EntityToDTO.getInstance().getCityEsDTO(cityBean);
				updateCityEsDTO(cityBean, cityEsDto, field);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cityEsDto;
	}

	private void updateCityEsDTO(CityEsBean cityBean, CityEsDTO cityEsDto, PropertyEnum field) {
		if (field == null) {
			cityEsDto.setSubwayDto(cityBean.getSubway());
			cityEsDto.setElevationRefDto(cityBean.getElevationRef());
			cityEsDto.setNetworkDto(cityBean.getNetwork());
			cityEsDto.setPopulationRefDto(cityBean.getPopulationRef());
			cityEsDto.setLanguagesRefDto(cityBean.getLanguagesRef());
			cityEsDto.setUvDto(cityBean.getUvDetails());
			cityEsDto.setWeatherDto(cityBean.getWeather());
			cityEsDto.setVegetationDto(cityBean.getVegetation());
		} else {
			switch (field) {
			case SUBWAY:
				cityEsDto.setSubwayDto(cityBean.getSubway());
				break;
			case ELEVATION:
				cityEsDto.setElevationRefDto(cityBean.getElevationRef());
				break;
			case NETWORK:
				cityEsDto.setNetworkDto(cityBean.getNetwork());
				break;
			case POPULATION:
				cityEsDto.setPopulationRefDto(cityBean.getPopulationRef());
				break;
			case LANGUAGE:
				cityEsDto.setLanguagesRefDto(cityBean.getLanguagesRef());
				break;
			case UVDATA:
				cityEsDto.setUvDto(cityBean.getUvDetails());
				break;
			case WEATHER:
				cityEsDto.setWeatherDto(cityBean.getWeather());
				break;
			case VEGETATION:
				cityEsDto.setVegetationDto(cityBean.getVegetation());
			case HOLIDAY:
				cityEsDto.setHolidays(cityBean.getHolidays());
			case AIRPORT:
				cityEsDto.setAirports(cityBean.getAirports());
				break;
			default:
				break;
			}
		}
	}

	@Override
	public CityDTO searchCityByName(String asciiName) {
		CityDTO cityDto = null;
		try {
			SearchResponse response = geoDataSearchDao.cityByName(asciiName);
			if (response != null && response.getHits().getTotalHits() > 0) {
				CityEsBean cityBean = EntityMapper.getInstance()
						.getObject(response.getHits().getHits()[0].getSourceAsString(), CityEsBean.class);
				cityDto = EntityToDTO.getInstance().getCityBean(cityBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cityDto;
	}

	@Override
	public List<GeoDTO> geoList(String countryName) {
		List<GeoDTO> geodto = null;
		try {
			List<EsQuery> geoSearchQuery = new ArrayList<>();
			EsQuery query = new EsQuery(fieldConfig.getGeoCountryName(), countryName, EsModeQuery.QUERY_STRING_QUERY,
					EsQueryIntMode.SHOULD, true);
			geoSearchQuery.add(query);

			QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(geoSearchQuery, EsQueryIntMode.SHOULD);

			SearchResponse response = entityManager.executeQuery(searchQueryBuilder, null, 0, 10000,
					masterConfig.getGeoCityIndexName());
			if (response != null && response.getHits().getHits().length > 0) {
				geodto = processGeoResponse(response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return geodto;
	}

	@Override
	public List<CityDTO> searchCityByCountry(String asciiName, String country, String countryCode) {
		List<CityDTO> citydto = null;
		try {
			int size = 5;
			if (countryCode.length() > 0 || country.length() > 0) {
				size = 1;
			}

			QueryBuilder countryQueryBuilder = null;

			List<EsQuery> geoSearchQuery = new ArrayList<>();
			EsQuery query = new EsQuery(fieldConfig.getGeoAsciiName(), asciiName, EsModeQuery.FUNCTION_SCORE,
					EsQueryIntMode.SHOULD, true);
			geoSearchQuery.add(query);

			query = new EsQuery(fieldConfig.getGeoAsciiName(), asciiName, EsModeQuery.MATCH, EsQueryIntMode.SHOULD,
					true);
			geoSearchQuery.add(query);

			query = new EsQuery(fieldConfig.getGeoAlterNativeNames(), asciiName, EsModeQuery.QUERY_STRING_QUERY,
					EsQueryIntMode.SHOULD, true);
			geoSearchQuery.add(query);

			QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(geoSearchQuery, EsQueryIntMode.SHOULD);

			if (countryCode.length() > 0) {
				geoSearchQuery.clear();
				query = new EsQuery(fieldConfig.getGeoCountryCode(), countryCode, EsModeQuery.MATCH,
						EsQueryIntMode.SHOULD);
				geoSearchQuery.add(query);

				query = new EsQuery(fieldConfig.getGeoadmin1Code(), countryCode, EsModeQuery.MATCH,
						EsQueryIntMode.SHOULD);
				geoSearchQuery.add(query);

				query = new EsQuery(fieldConfig.getGeoState(), countryCode, EsModeQuery.MATCH, EsQueryIntMode.SHOULD);
				geoSearchQuery.add(query);
				countryQueryBuilder = QueryBuilderHelper.search(geoSearchQuery, EsQueryIntMode.SHOULD);
			}

			QueryBuilder mergedBuilder = QueryBuilderHelper.search(EsQueryIntMode.MUST, searchQueryBuilder,
					countryQueryBuilder);
			SearchResponse response = entityManager.executeQuery(mergedBuilder, null, 0, size,
					masterConfig.getGeoCityIndexName());
			if (response != null && response.getHits().getHits().length > 0) {
				citydto = processCityResponse(response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return citydto;
	}

	@Override
	public List<CityDTO> searchCityByGeoPoint(final String geoPoint, final String distance) {
		List<CityDTO> citydto = null;
		try {
			List<CityEsBean> cityList = geoDataSearchDao.searchCityByGeoPoint(geoPoint, distance);
			if (CollectionUtils.isNotEmpty(cityList)) {
				citydto = processCityList(cityList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return citydto;
	}

	private List<CityDTO> processCityList(List<CityEsBean> cityList) throws Exception {
		List<CityDTO> cityDtoList = new ArrayList<>();
		for (CityEsBean cityBean : cityList) {
			CityDTO cityDto = EntityToDTO.getInstance().getCityBean(cityBean);
			cityDtoList.add(cityDto);
		}
		return cityDtoList;
	}

	@Override
	public List<CitySuggestDTO> citySuggest(String asciiName) {
		Map<Object, CitySuggestDTO> citySuggestMap = new LinkedHashMap<>();
		List<CitySuggestDTO> citySuggestList = new ArrayList<>();
		try {
			SearchResponse searchResponse = geoDataSearchDao.citySuggest(asciiName, "asciiSuggest");
			CompletionSuggestion asciiCompletionSuggestion = searchResponse.getSuggest().getSuggestion("asciiSuggest");
			extractSuggestResults(asciiCompletionSuggestion, citySuggestMap);
			if (citySuggestMap.size() < 10) {
				SearchResponse alternateSearchResponse = geoDataSearchDao.citySuggest(asciiName,
						"alternateNamesSuggest");
				CompletionSuggestion alternateSuggestion = alternateSearchResponse.getSuggest()
						.getSuggestion("alternateNamesSuggest");
				extractSuggestResults(alternateSuggestion, citySuggestMap);
			}
			citySuggestMap.values().stream().forEach(cityDto -> citySuggestList.add(cityDto));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return citySuggestList;
	}

	@Override
	public JetLagInfo getJetLag(Long toGeoId, Long fromGeoId) {
		JetLagInfo jetLagInfo = null;
		try {
			List<EsQuery> geoSearchQuery = QueryBuilderHelper.getSearchQuery(Lists.newArrayList(toGeoId, fromGeoId),
					Lists.newArrayList(fieldConfig.getGeoNameId()));
			QueryBuilder searchQueryBuilder = QueryBuilderHelper.search(geoSearchQuery, EsQueryIntMode.MUST);
			SearchResponse response = entityManager.executeQuery(searchQueryBuilder, null, 0, 20,
					masterConfig.getGeoCityIndexName());
			if (response != null && response.getHits().getTotalHits() == 2) {
				Map<Long, CityEsBean> cityBeanMap = getCityBeanMap(response);
				jetLagInfo = processCityMap(toGeoId, fromGeoId, cityBeanMap);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jetLagInfo;
	}

	/**
	 * Process city map.
	 *
	 * @param toGeoId
	 *            the to geo id
	 * @param fromGeoId
	 *            the from geo id
	 * @param cityMap
	 *            the city map
	 * @return the jet lag info
	 */
	private JetLagInfo processCityMap(final Long toGeoId, final Long fromGeoId, Map<Long, CityEsBean> cityMap) {
		JetLagInfo jetLagInfo = new JetLagInfo();
		String toTimeZone = cityMap.get(toGeoId).getTimeZoneUtc();
		String fromTimeZone = cityMap.get(fromGeoId).getTimeZoneUtc();

		jetLagInfo.setToTimeZone(toTimeZone);
		jetLagInfo.setFromTimeZone(fromTimeZone);
		float timeZoneDifference = WooApiUtils.timeZoneDifference(toTimeZone, fromTimeZone);
		jetLagInfo.setTimeZoneDifference(timeZoneDifference);

		String direction = timeZoneDifference > 0 ? "EAST" : "WEST";
		jetLagInfo.setDirection(direction);

		float recoveryTime;
		if ("EAST".equalsIgnoreCase(direction)) {
			int noofTimeZoneCrossed = (int) (timeZoneDifference / 0.5);
			recoveryTime = Float.valueOf(noofTimeZoneCrossed / 2f);
		} else {
			int noofTimeZoneCrossed = (int) (timeZoneDifference / 0.5);
			recoveryTime = Float.valueOf(noofTimeZoneCrossed / 2.5f);
		}
		jetLagInfo.setRecoveryTime(recoveryTime);
		return jetLagInfo;
	}

	/**
	 * Gets the city bean map.
	 *
	 * @param response
	 *            the response
	 * @return the city bean map
	 * @throws Exception
	 *             the exception
	 */
	private Map<Long, CityEsBean> getCityBeanMap(SearchResponse response) throws Exception {
		Map<Long, CityEsBean> cityBeanMap = new HashMap<>();
		SearchHit[] dataList = response.getHits().getHits();
		for (SearchHit hit : dataList) {
			CityEsBean cityBean = EntityMapper.getInstance().getObject(hit.getSourceAsString(), CityEsBean.class);
			cityBeanMap.put(cityBean.getGeonameId(), cityBean);
		}
		return cityBeanMap;
	}

	@Override
	public GeoIPLocationResponse searchCityByIP(String ipAddress) {
		String url = geoIPExtracter.getGeoSearchURL(geoConfig.getGeoIPSearchURl(), ipAddress);
		GeoIPLocationResponse geoResponse = null;
		try {
			geoResponse = geoIPExtracter.getGeoResponse(url);
			if (StringUtils.isNotEmpty(geoResponse.getCity())) {
				List<CityDTO> cityList = searchCityByCountry(geoResponse.getCity(), geoResponse.getCountry_name(),
						geoResponse.getCountry_code());
				if (CollectionUtils.isNotEmpty(cityList)) {
					geoResponse.setGeoNameId(cityList.get(0).getGeonameId());
				}
			} else {
				String geoPoint = geoResponse.getLatitude() + "," + geoResponse.getLongitude();
				List<CityDTO> cityList = searchCityByGeoPoint(geoPoint, "10");
				if (CollectionUtils.isNotEmpty(cityList)) {
					CityDTO dto = cityList.get(0);
					geoResponse = new GeoIPLocationResponse();
					geoResponse.setGeoNameId(dto.getGeonameId());
					geoResponse.setCity(dto.getName());
					geoResponse.setState(dto.getState());
					geoResponse.setCountry_name(dto.getCountryName());
					geoResponse.setCountry_code(dto.getCountryCode());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return geoResponse;
	}

	/**
	 * Process geo response.
	 *
	 * @param response
	 *            the response
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	private List<GeoDTO> processGeoResponse(SearchResponse response) throws Exception {
		List<GeoDTO> geoDtoList = new ArrayList<>();
		SearchHit[] dataList = response.getHits().getHits();
		for (SearchHit hit : dataList) {
			CityEsBean cityBean = EntityMapper.getInstance().getObject(hit.getSourceAsString(), CityEsBean.class);
			geoDtoList.add(EntityToDTO.getInstance().getGeoBean(cityBean));
		}
		return geoDtoList;
	}

	/**
	 * Extract suggest results.
	 *
	 * @param skilCompletionSuggestion
	 *            the skil completion suggestion
	 * @param cityMap
	 *            the city map
	 * @throws Exception
	 */
	private void extractSuggestResults(CompletionSuggestion skilCompletionSuggestion,
			Map<Object, CitySuggestDTO> cityMap) throws Exception {
		List<CompletionSuggestion.Entry> entries = skilCompletionSuggestion.getEntries();
		if (!entries.isEmpty()) {
			CompletionSuggestion.Entry entry = entries.get(0);
			List<Option> options = entry.getOptions();
			for (Option option : options) {
				String key = option.getText().toString();
				Map<String, Object> city = option.getHit().getSourceAsMap();
				Object geoNameId = city.get("geonameId");
				cityMap.put(geoNameId, getCitySuggestBean(city, key, geoNameId));
			}
		}
	}

	/**
	 * Gets the city suggest bean.
	 *
	 * @param city
	 *            the city
	 * @param searchMatchedKey
	 *            the search matched key
	 * @param geoNameId
	 *            the geo name id
	 * @return the city suggest bean
	 * @throws Exception
	 */
	private CitySuggestDTO getCitySuggestBean(Map<String, Object> city, String searchMatchedKey, Object geoNameId)
			throws Exception {
		String cityName = city.get("asciiName").toString();
		String state = city.get("state").toString();
		String countryName = city.get("countryName").toString();
		String countryCode = city.get("countryCode").toString();
		Object population = city.get("population");
		Object location = city.get("geoLocation");

		CitySuggestDTO citySuggestBean = new CitySuggestDTO(geoNameId, cityName, state, countryCode,
				city.get("countryCodeIso3").toString(), countryName);

		citySuggestBean.setSearchMatchedKey(searchMatchedKey);
		citySuggestBean.setPopulation(population);
		String fullName = cityName + ", " + state + ", " + countryName;
		citySuggestBean.setFullName(fullName);
		citySuggestBean.setGeoLocation(location);
		return citySuggestBean;
	}

	private List<CityDTO> processCityResponse(SearchResponse response) throws Exception {
		List<CityDTO> cityDtoList = new ArrayList<>();
		SearchHit[] dataList = response.getHits().getHits();
		for (SearchHit hit : dataList) {
			CityEsBean cityBean = EntityMapper.getInstance().getObject(hit.getSourceAsString(), CityEsBean.class);
			CityDTO cityDto = EntityToDTO.getInstance().getCityBean(cityBean);
			cityDtoList.add(cityDto);
		}
		return cityDtoList;
	}
}

package com.woocation.service.impl;

import java.io.File;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.woocation.configuration.MasterDataConfig;
import com.woocation.elastic.core.ElasticEntityManager;
import com.woocation.model.CityEsBean;
import com.woocation.model.Location;
import com.woocation.service.GeoDataUploadService;
import com.woocation.service.request.DataUploadRequest;
import com.woocation.service.response.GeoDataUploadResponse;
import com.woocation.util.WooApiUtils;

/**
 * The Class GeoDataUploadServiceImpl.
 */
@Service
public class GeoDataUploadServiceImpl implements GeoDataUploadService {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(GeoDataUploadServiceImpl.class);

	/** The geo config. */
	@Autowired
	private MasterDataConfig geoConfig;

	/** The entity manager. */
	@Autowired
	private ElasticEntityManager entityManager;

	@Override
	public GeoDataUploadResponse uploadGeoData(DataUploadRequest uploadRequest) {
		GeoDataUploadResponse response = new GeoDataUploadResponse();
		if (uploadRequest.getDataType().equalsIgnoreCase("City")) {
			try {
				uploadGeoData(uploadRequest.getFileLocation(), geoConfig.getGeoCityIndexName(),
						geoConfig.getGeoCitydocType());
				response.setStatusMessage("Data uploaded success");
				response.setStatus(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {

		}
		return response;
	}

	/**
	 * Upload geo data.
	 *
	 * @param path
	 *            the path
	 * @param geoIndexName
	 *            the geo index name
	 * @param docType
	 *            the doc type
	 * @return true, if successful
	 * @throws Exception
	 *             the exception
	 */
	private boolean uploadGeoData(final String path, final String geoIndexName, final String docType) throws Exception {
		boolean uploadStatus = false;
		try {
			if (entityManager.validateIndex(geoIndexName, docType, CityEsBean.class)) {
				List<File> filesInFolder = Files.walk(Paths.get(path)).filter(Files::isRegularFile).map(Path::toFile)
						.collect(Collectors.toList());
				for (File file : filesInFolder) {
					// List<CityEsBean> cityList =
					// getGeoCityListGeneric(file.getAbsolutePath());
					List<CityEsBean> cityList = readCityFile(file.getAbsolutePath());
					if (cityList != null) {
						entityManager.saveAll(cityList, geoIndexName, docType);
					}
				}
				uploadStatus = true;
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
		return uploadStatus;
	}

	/**
	 * Gets the geo city list.
	 *
	 * @param path
	 *            the path
	 * @return the geo city list
	 * @throws Exception
	 *             the exception
	 */
	private List<CityEsBean> getGeoCityListGeneric(final String path) throws Exception {
		List<CityEsBean> list = new ArrayList<>();
		long startTime = System.currentTimeMillis();
		List<String> allLines = java.nio.file.Files.readAllLines(new File(path).toPath());
		System.out.println(System.currentTimeMillis() - startTime);
		allLines.stream().forEach(line -> list.add(getCityEsBean(line)));
		return list;
	}

	/**
	 * Read subway file.
	 */
	public List<CityEsBean> readCityFile(String filePath) {
		try {
			return readFile(filePath, CityEsBean.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public <T> List<T> readFile(String fileName, Class<T> pojoClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		List<T> list = null;
		try {
			File jsonFile = new File(fileName);
			list = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, pojoClass));
			System.out.println("Records Read in File --> " + fileName + " --> " + list.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * Gets the city es bean.
	 *
	 * @param dataRow
	 *            the data row
	 * @return the city es bean
	 */
	private CityEsBean getCityEsBean(String dataLine) {
		CityEsBean cityBean = new CityEsBean();
		try {
			String[] dataRow = dataLine.split("\t");
			cityBean.setGeonameId(Long.valueOf(dataRow[0]));
			cityBean.setName(dataRow[1]);
			cityBean.setAsciiName(dataRow[2]);
			cityBean.setAlternateNames(dataRow[3]);
			Location point = new Location(Double.parseDouble(dataRow[4]), Double.parseDouble(dataRow[5]));
			cityBean.setGeoLocation(point);
			cityBean.setFeatureClass(dataRow[6]);
			cityBean.setFeatureCode(dataRow[7]);
			cityBean.setCountryCode(dataRow[8]);
			cityBean.setCc2(dataRow[9]);
			cityBean.setAdmin1Code(dataRow[10]);
			cityBean.setAdmin2Code(dataRow[11]);
			cityBean.setAdmin3Code(dataRow[12]);
			cityBean.setAdmin4Code(dataRow[13]);
			cityBean.setPopulation(BigInteger.valueOf(Long.valueOf(dataRow[14])));
			cityBean.setElevation(Strings.isNullOrEmpty(dataRow[15]) ? 0 : Integer.valueOf(dataRow[15]));
			cityBean.setDem(Integer.valueOf(dataRow[16]));
			cityBean.setTimeZone(dataRow[17]);
			cityBean.setTimeZoneUtc(WooApiUtils.utcTimeZone(dataRow[17]));
			cityBean.setModificationDate(dataRow[18]);
			cityBean.setState(dataRow[19]);
			cityBean.setCountryName(dataRow[20]);
			cityBean.setCountryCodeIso3(dataRow[21]);
			cityBean.setCurrencyName(dataRow[22]);
			cityBean.setCurrencyCode(dataRow[23]);
			cityBean.setEuropean(Boolean.valueOf(dataRow[24]));
			cityBean.setSchengen(Boolean.valueOf(dataRow[25]));

			cityBean.setSuggest();
		} catch (NumberFormatException e) {
			System.out.println("Error -> " + dataLine);
		}
		return cityBean;
	}
}

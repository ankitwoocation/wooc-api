package com.woocation.service.response;

/**
 * The Class GeoDataUploadResponse.
 */
public class GeoDataUploadResponse {

	/** The status message. */
	private String statusMessage = "";

	/** The status. */
	private boolean status = false;

	/**
	 * Gets the status message.
	 *
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * Sets the status message.
	 *
	 * @param statusMessage
	 *            the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * Checks if is status.
	 *
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status
	 *            the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

}

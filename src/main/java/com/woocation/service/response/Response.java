package com.woocation.service.response;

import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Class Response.
 *
 * @param <T>
 *            the generic type
 */
@Data
@NoArgsConstructor
public class Response<T> {

	/** The results. */
	private List<T> results;

	/** The http status. */
	private HttpStatus httpStatus;

	/** The message. */
	private String message;

	/**
	 * Instantiates a new response.
	 *
	 * @param results
	 *            the results
	 * @param httpStatus
	 *            the http status
	 * @param message
	 *            the message
	 */
	public Response(List<T> results, HttpStatus httpStatus, String message) {
		this.results = results;
		this.httpStatus = httpStatus;
		this.message = message;
	}

	/**
	 * Instantiates a new response.
	 *
	 * @param httpStatus
	 *            the http status
	 * @param message
	 *            the message
	 */
	public Response(HttpStatus httpStatus, String message) {
		super();
		this.httpStatus = httpStatus;
		this.message = message;
	}
}

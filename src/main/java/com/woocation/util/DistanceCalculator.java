package com.woocation.util;

import com.woocation.model.Location;

/**
 * The Class DistanceCalculator.
 */
public class DistanceCalculator {

	/**
	 * Distance.
	 *
	 * @param fromLocation
	 *            the from location
	 * @param toLocation
	 *            the to location
	 * @param unit
	 *            the unit
	 * @return the double
	 */
	public static Integer distance(Location fromLocation, Location toLocation, String unit) {
		double theta = fromLocation.getLon() - toLocation.getLon();
		double dist = Math.sin(deg2rad(fromLocation.getLat())) * Math.sin(deg2rad(toLocation.getLat()))
				+ Math.cos(deg2rad(fromLocation.getLat())) * Math.cos(deg2rad(toLocation.getLat()))
						* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}

		return (int)dist;
	}

	/**
	 * Deg 2 rad.
	 *
	 * @param deg
	 *            the deg
	 * @return the double
	 */
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/**
	 * Rad 2 deg.
	 *
	 * @param rad
	 *            the rad
	 * @return the double
	 */
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
}
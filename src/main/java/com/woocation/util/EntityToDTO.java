package com.woocation.util;

import org.modelmapper.ModelMapper;

import com.woocation.dto.AirportDTO;
import com.woocation.dto.CityDTO;
import com.woocation.dto.CityEsDTO;
import com.woocation.dto.GeoDTO;
import com.woocation.model.Airport;
import com.woocation.model.CityEsBean;

/**
 * The Class EntityToDTO.
 */
public final class EntityToDTO {
	/**
	 * The unique instance.
	 */
	private static EntityToDTO instance;

	/**
	 * The model mapper.
	 */
	private ModelMapper mapper = new ModelMapper();

	/**
	 * Default constructor.
	 */
	private EntityToDTO() {
		initConfig();
	}

	/**
	 * Initialize.
	 */
	private void initConfig() {
	}

	/**
	 * Gets the unique instance.
	 * 
	 * @return the unique instance.
	 */
	public static EntityToDTO getInstance() {
		if (instance == null) {
			instance = new EntityToDTO();
		}
		return instance;
	}

	/**
	 * Gets the city bean.
	 *
	 * @param cityEsBean
	 *            the city es bean
	 * @return the city bean
	 */
	public CityDTO getCityBean(CityEsBean cityEsBean) {
		return mapper.map(cityEsBean, CityDTO.class);
	}

	/**
	 * Gets the geo bean.
	 *
	 * @param cityEsBean
	 *            the city es bean
	 * @return the geo bean
	 */
	public GeoDTO getGeoBean(CityEsBean cityEsBean) {
		return mapper.map(cityEsBean, GeoDTO.class);
	}

	/**
	 * Gets the geo bean.
	 *
	 * @param cityEsBean
	 *            the city es bean
	 * @return the geo bean
	 */
	public CityEsDTO getCityEsDTO(CityEsBean cityEsBean) {
		return mapper.map(cityEsBean, CityEsDTO.class);
	}

	/**
	 * Gets the aiport DTO.
	 *
	 * @param airport
	 *            the airport
	 * @return the aiport DTO
	 */
	public AirportDTO getAiportDTO(Airport airport) {
		return mapper.map(airport, AirportDTO.class);
	}
}

/**
 * 
 */
package com.woocation.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.woocation.elastic.core.EntityMapper;
import com.woocation.exception.ElasticException;
import com.woocation.service.response.GeoIPLocationResponse;

/**
 * @author Ankit.Gupta
 *
 */
@Component
public class GeoIPExtractor {

	@Autowired
	private RestTemplate restTemplate;
	
	/**
	 * Gets the geo response.
	 *
	 * @param url
	 *            the url
	 * @param clsResponse
	 *            the cls response
	 * @return the geo response
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 */
	public GeoIPLocationResponse getGeoResponse(String url) {
		GeoIPLocationResponse geoLocation = null;
		try {
			String response = restTemplate.getForObject(url, String.class);
			geoLocation = getGeoIPLocationResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return geoLocation;
	}

	/**
	 * Gets the geo search URL.
	 *
	 * @param ipAddress
	 *            the ip address
	 * @return the geo search URL
	 */
	public String getGeoSearchURL(String geoIPSearchURL, String ipAddress) {
		String url = "";
		String envUrl = System.getenv("GEO_URL");
		if (envUrl != null && envUrl.trim().length() > 0) {
			url = envUrl + ipAddress;
		} else {
			url = geoIPSearchURL + ipAddress;
		}
		return url;
	}

	/**
	 * Gets the geo IP location response.
	 *
	 * @param serviceResponse
	 *            the service response
	 * @return the geo IP location response
	 * @throws ElasticException
	 *             the elastic exception
	 */
	public GeoIPLocationResponse getGeoIPLocationResponse(String serviceResponse) throws Exception {
		String response = serviceResponse.replace("callback(", "");
		String json = response.substring(0, response.length() - 1);
		return EntityMapper.getInstance().getObject(json, GeoIPLocationResponse.class);
	}

}

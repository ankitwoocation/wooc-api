package com.woocation.util;

/**
 * The Enum PropertyEnum.
 */
public enum PropertyEnum {

	/** The subway. */
	SUBWAY("SUBWAY"),

	/** The elevation. */
	ELEVATION("ELEVATION"),

	/** The network. */
	NETWORK("NETWORK"),

	/** The population. */
	POPULATION("POPULATION"),

	/** The language. */
	LANGUAGE("LANGUAGE"),

	/** The uvdata. */
	UVDATA("UV"),

	/** The vegetation. */
	VEGETATION("VEGETATION"),

	/** The weather. */
	WEATHER("WEATHER"),
	
	/** The holiday. */
	HOLIDAY("HOLIDAY"),
	
	/** The airport. */
	AIRPORT("AIRPORT");

	/** The property. */
	private String property;

	/**
	 * Instantiates a new property enum.
	 *
	 * @param property the property
	 */
	private PropertyEnum(String property) {
		this.property = property;
	}
}